package be.ordina.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class Browser {
    public static WebDriver chrome(){
        System.setProperty("webdriver.chrome.driver", "/Users/RiVe/Documents/chromedriver.exe");
        return new ChromeDriver();
    }


    public static WebDriver edge(){
        System.setProperty("webdriver.edge.driver", "/Users/RiVe/Documents/msedgedriver.exe");
        return new EdgeDriver();
    }


}

