package be.ordina.utils;

import org.openqa.selenium.WebDriver;


public class StartSiteInBrowser {

    private WebDriver driver;


    public StartSiteInBrowser() {

        this.driver = driver;
    }


    public static WebDriver chrome(WebDriver driver){

        driver = Browser.chrome();
        driver.get("https://www.plex.tv/");
        driver.manage().window().maximize();

        return driver;
    }


    public static WebDriver edge(WebDriver driver){

        driver = Browser.edge();
        driver.get("https://www.plex.tv/");
        driver.manage().window().maximize();

        return driver;
    }


}

