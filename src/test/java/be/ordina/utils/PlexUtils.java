package be.ordina.utils;

import be.ordina.pages.LoginModal;
import be.ordina.pages.StartPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PlexUtils {


    public static void loginAndLaunch(WebDriver driver, WebDriverWait wait){
        By btnLaunch = By.xpath("//a[@class='launch button']");

        StartPage startPage = new StartPage(driver);
        startPage.clickSignIn();

        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));

        LoginModal loginModal = new LoginModal(driver);
        loginModal.signIn("PlexEmailRia@mailinator.com", "testPaswoord1");

        driver.switchTo().parentFrame();

        wait.until(ExpectedConditions.visibilityOfElementLocated(btnLaunch));
        driver.findElement(btnLaunch).click();
    }


    public static void signupAndLaunch(WebDriver driver, WebDriverWait wait)  {
        StartPage startPage = new StartPage(driver);
        LoginModal loginModal = new LoginModal(driver);
        startPage.clickSignUp();

        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));

        String inputEmail = getAlphanumericString(8) + "@mailinator.com";
        loginModal.signUp(inputEmail, "testPaswoord1");

        driver.switchTo().parentFrame();
    }


    public static String getAlphanumericString(int n){
        // chose a character random from this string
        String AlphaNumericString = "ABCEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789" + "abcdefghijklmnopqrstuvwxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i<n; i++){
            int index = (int) (AlphaNumericString.length()
                    * Math.random() );

            // add character one by one in end of sb
            sb.append(AlphaNumericString.charAt(index));
        }
        return sb.toString();
    }
}
