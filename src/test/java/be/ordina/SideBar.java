package be.ordina;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SideBar {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By btnSideBar = By.xpath("//a[contains(@class,'SourceSidebarLink-sourceLink')]//div[contains(@class,'SourceSidebarLink-title-')]");


    public SideBar(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,20);
    }


    public void selectSideBarTitle(String txtElementSideBar){
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnSideBar));

        List<WebElement> elementsSideBar = driver.findElements(btnSideBar);
        for (int i = 0; i<= elementsSideBar.size(); i++){
            String btnSideBarName = elementsSideBar.get(i).getText();
            if (btnSideBarName.contains(txtElementSideBar) && elementsSideBar.get(i).isDisplayed()){
                elementsSideBar.get(i).click();
                break;
            }
        }

    }

}

