package be.ordina;

import be.ordina.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

public class AddMovieToWatchlist {

        public static String addMovie(WebDriver driver, WebDriverWait wait, Integer ix) throws IOException {
                By inputSearch = By.xpath("//input[contains(@class,'QuickSearchInput-searchInput')]");
                By detailTitle = By.xpath("//div[contains(@class,'PrePlayLeftTitle-leftTitle')]/span");
                By btnAddToWatchlist = By.xpath("//button[contains(@data-qa-id,'preplay-addToWatchlist')]");

                wait.until(ExpectedConditions.visibilityOfElementLocated(inputSearch));
                driver.findElement(inputSearch).sendKeys("story");

                By resultXTitle = By.xpath("//div[h5[text()='Movies']]/div[contains(@data-qa-id,'quickSearchItemButton')]["+ix+"]/a");
                wait.until(ExpectedConditions.visibilityOfElementLocated(resultXTitle));
                driver.findElement(resultXTitle).click();
                wait.until(ExpectedConditions.elementToBeClickable(btnAddToWatchlist));
                SeleniumUtils.screenshot(driver, "watchlist-add-movie" + ix);
                String detailMovieTitle = driver.findElement(detailTitle).getAttribute("title");

                driver.findElement(btnAddToWatchlist).click();

                return detailMovieTitle;
        }


}