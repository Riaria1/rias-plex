package be.ordina.pages;

import be.ordina.SideBar;
import be.ordina.utils.SeleniumUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;

public class MoviesAndShowsPage {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By tabMoviesAndShows = By.xpath("//div[contains(@class,'SourceSidebarItem-button')]/a[contains(@title,'Movies & Shows')]");
    private By sidebarWatchlist = By.xpath("//div[contains(@class,'Measure-container')]//a[text()='Watchlist']");
    private By watchlistTitle = By.xpath("//div[contains(@class,'MetadataPosterCard-cardContainer')]");
    private By sidebarCategories = By.xpath("//div[contains(@class,'Measure-container')]//a[text()='Categories']");
    private By categoryTitle = By.xpath("//div[contains(@class,'MetadataDirectoryPosterCell-titleContainer')]//span");

    public MoviesAndShowsPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void viewWatchlist(String txtElementSideBar) throws IOException {
        SideBar sideBar = new SideBar(driver);
        sideBar.selectSideBarTitle(txtElementSideBar);

        wait.until(ExpectedConditions.elementToBeClickable(sidebarWatchlist));
        driver.findElement(sidebarWatchlist).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(watchlistTitle));
        SeleniumUtils.screenshot(driver, "watchlist-list-movies");
    }


    public void selectCategoriesToFile(String txtElementSideBar) throws IOException {
        SideBar sideBar = new SideBar(driver);
        sideBar.selectSideBarTitle(txtElementSideBar);

        wait.until(ExpectedConditions.elementToBeClickable(sidebarCategories));
        driver.findElement(sidebarCategories).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(categoryTitle));
        List<WebElement> elementsCategory = driver.findElements(categoryTitle);

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        System.out.println(formatter.format(date));
        File file = new File("target/files/category.txt");
        FileUtils.writeStringToFile(file, "Categories on " + date + "\n\n","UTF8",false);

        for (int i = 0; i< elementsCategory.size(); i++){
            String categoryTitleName = elementsCategory.get(i).getText();
            if (elementsCategory.get(i).isDisplayed()){
                FileUtils.writeStringToFile(file, categoryTitleName+"\n","UTF8",true);
            }
        }

    }

}