package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StartPage {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By btnSignUp = By.xpath("//a[@class='signup button']");
    private By btnSignIn = By.xpath("//a[@class='signin']");


    public StartPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void clickSignUp(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnSignUp));
        driver.findElement(btnSignUp).click();
    }

    public void clickSignIn(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnSignIn));
        driver.findElement(btnSignIn).click();
    }

}