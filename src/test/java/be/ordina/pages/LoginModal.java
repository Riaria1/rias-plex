package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginModal {

    private final WebDriver driver;
    private WebDriverWait wait;

    private By tbEmail = By.xpath("//input[@id='email']");
    private By tbPassword = By.xpath("//input[@id='password']");
    private By btnLogIn = By.xpath("//button[@tabindex='3']");
    private By btnCreateAccount = By.xpath("//button[@type='submit']");


    public LoginModal(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void signIn(String email, String password){
        wait.until(ExpectedConditions.visibilityOfElementLocated(tbEmail));
        driver.findElement(tbEmail).sendKeys(email);
        driver.findElement(tbPassword).sendKeys(password);

        driver.findElement(btnLogIn).click();
    }

    public void signUp(String email, String password){
        wait.until(ExpectedConditions.visibilityOfElementLocated(tbEmail));
        driver.findElement(tbEmail).sendKeys(email);
        driver.findElement(tbPassword).sendKeys(password);

        driver.findElement(btnCreateAccount).click();
    }

}
