package be.ordina.pages;

import be.ordina.SideBar;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;


public class HomePage {
    private final WebDriver driver;
    private WebDriverWait wait;


    public HomePage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void viewHomePage(String txtElementSideBar) throws IOException {
        SideBar sideBar = new SideBar(driver);
        sideBar.selectSideBarTitle(txtElementSideBar);
    }

}