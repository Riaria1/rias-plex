package be.ordina;

import be.ordina.pages.HomePage;
import be.ordina.pages.MoviesAndShowsPage;
import be.ordina.utils.PlexUtils;
import be.ordina.utils.SeleniumUtils;
import be.ordina.utils.StartSiteInBrowser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;

import static org.junit.Assert.*;

/**
Automatic test for Plex
 */

public class AppTest 
{
    private WebDriver driver;
    private WebDriverWait wait;

    private final By posterFirstMovie = By.xpath("//a[contains(@class,'PosterCardLink-link')]");
    private final By btnPlay = By.xpath("//div[contains(@class,'MetadataPosterListItem-card')]//button[@aria-label='Play']");
    private final By iconPause = By.id("plex-icon-player-pause-560");
    private final By playerControlsBar = By.xpath("//div[@data-qa-id='playerControlsContainer']");
    private final By btnAgree = By.xpath("//div[contains(@class,'ModalFooterButtonBar-buttonBar')]//span[text()='I Agree']");
    private final By inputSearch = By.xpath("//input[contains(@class,'QuickSearchInput-searchInput')]");
    private final By detailTitle = By.xpath("//div[contains(@class,'PrePlayLeftTitle-leftTitle')]/span");
    private final By resultSecondTitle = By.xpath("//div[h5[text()='Movies']]/div[contains(@data-qa-id,'quickSearchItemButton')][2]/a");


    @Before
    public void setUp(){
        //driver = StartSiteInBrowser.chrome(driver);
        driver = StartSiteInBrowser.edge(driver);

        this.wait = new WebDriverWait(driver, 10);
    }


    @After
    public void tearDown(){
        driver.close();
    }


    @Test
    public void successfulSignUp() throws IOException {
        PlexUtils.signupAndLaunch(driver, wait);

        HomePage homePage = new HomePage(driver);
        homePage.viewHomePage("Home");

        wait.until(ExpectedConditions.visibilityOfElementLocated(posterFirstMovie));
        SeleniumUtils.screenshot(driver, "SignUp");
    }


    @Test
    public void successfulLogIn() throws IOException {
        PlexUtils.loginAndLaunch(driver, wait);

        HomePage homePage = new HomePage(driver);
        homePage.viewHomePage("Home");

        wait.until(ExpectedConditions.visibilityOfElementLocated(posterFirstMovie));
        SeleniumUtils.screenshot(driver, "LogIn");
    }


    @Test
    public void successfulStartStreaming() throws IOException {
        PlexUtils.signupAndLaunch(driver, wait);

        wait.until(ExpectedConditions.visibilityOfElementLocated(posterFirstMovie));
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(posterFirstMovie)).perform();

        wait.until(ExpectedConditions.visibilityOfElementLocated(btnPlay));
        SeleniumUtils.screenshot(driver, "startStreaming-playbutton");
        driver.findElement(btnPlay).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(btnAgree));
        driver.findElement(btnAgree).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(playerControlsBar));
        action.moveToElement(driver.findElement(playerControlsBar)).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(iconPause));
        SeleniumUtils.screenshot(driver, "startStreaming-play-movie");

        String ariaHidden = driver.findElement(iconPause).getAttribute("aria-hidden");
        assertTrue(ariaHidden.equals("true"));
    }


    @Test
    public void successfulSearch() throws IOException {
        PlexUtils.loginAndLaunch(driver, wait);

        wait.until(ExpectedConditions.visibilityOfElementLocated(inputSearch));
        driver.findElement(inputSearch).sendKeys("sum");
        wait.until(ExpectedConditions.visibilityOfElementLocated(resultSecondTitle));
        SeleniumUtils.screenshot(driver, "search-list-movies");
        String resultMovieTitle = driver.findElement(resultSecondTitle).getAttribute("title");
        driver.findElement(resultSecondTitle).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(detailTitle));
        SeleniumUtils.screenshot(driver, "search-detail-movie");
        String detailMovieTitle = driver.findElement(detailTitle).getAttribute("title");

        assertEquals(resultMovieTitle, detailMovieTitle);
    }


    @Test
    public void successfulAddToWatchlist() throws IOException {
        PlexUtils.signupAndLaunch(driver, wait);

        System.out.println("Movies in watchlist:");

        String detailMovieTitle2 = AddMovieToWatchlist.addMovie(driver, wait, 2);
        System.out.println("\t" + detailMovieTitle2);

        String detailMovieTitle3 = AddMovieToWatchlist.addMovie(driver, wait, 3);
        System.out.println("\t" + detailMovieTitle3);


        MoviesAndShowsPage moviesAndShowsPage = new MoviesAndShowsPage(driver);
        moviesAndShowsPage.viewWatchlist("Movies & Shows");

        String movieInWatchlist = driver.findElement(By.xpath("//div[@data-qa-id='cellItem']//a[contains(@title,'"+detailMovieTitle3+"')]")).getText();

        assertEquals(movieInWatchlist, detailMovieTitle3);
    }


    @Test
    public void successfulWriteCategoriesToFile() throws IOException {
        PlexUtils.signupAndLaunch(driver, wait);

        MoviesAndShowsPage moviesAndShowsPage = new MoviesAndShowsPage(driver);
        moviesAndShowsPage.selectCategoriesToFile("Movies & Shows");

        SeleniumUtils.screenshot(driver, "categories");
    }


    @Test
    public void failedTest()  {
        assertEquals("OK", "NOK");

    }

}
